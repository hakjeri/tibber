# Tibber code challenge

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

You need to create a `.env.local` with environment variables to access the Tibber graphql API and let the client application know where this application's host name. You can use the `.env.default` as a template.

To run locally:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the frontend. Pages are built using the folder structure, which can be found under `src/pages/`. The application uses hot module replacement.

[API routes](https://nextjs.org/docs/api-routes/introduction) are located under `src/pages/api/`, these are not treated as pages but rather as endpoints to be called for data.

## TODO

* Add error handling for apollo client
* Add automatic linter/prettier on commit
* Add unit tests for API
* Add Docker wrapper for easier and more generic deployment
* Add fun "magic button" :)