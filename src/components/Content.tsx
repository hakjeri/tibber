import React from "react"

export const Content = () => {
  return (
    <div className="col-span-12 md:col-span-6 md:col-start-4 px-4 mb-10">
      <h1>Infrastructure supply chain seed lean startup technology</h1>
      <p>Assets traction angel investor user experience social media leverage value proposition startup success founders creative. Equity value proposition launch party business-to-consumer research & development freemium bandwidth stock scrum project analytics.</p>
      <p>Agile development backing business-to-consumer analytics burn rate leverage business-to-business market creative responsive web design graphical user interface</p>
    </div>
  )
}
