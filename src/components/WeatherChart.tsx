import React from 'react';
import { format } from 'date-fns';

export const WeatherChart = ({entries, maxTemperature, minTemperature}: WeatherChartProps) => {
  if (!entries || entries.length !== 24) {
    return null
  }
  const averageTemperature = Math.round(entries.reduce((total, entry) => total + entry.temperature, 0) / 24)
  const ceilTemperature = Math.floor(maxTemperature)
  const flooredTemperature = Math.floor(minTemperature)
  
  const firstEntryDate = new Date(entries[0].time)
  const lastEntryDate = new Date(entries[entries.length -1].time)
  
  return (
    <div className="grid grid-cols-24 justify-items-center col-span-12 bg-dark-blue p-2 md:p-4 mb-10 text-white">
      <h2 className="col-span-24">Average temperature</h2>
      <span className="col-span-24 text-6xl mb-12">{`${averageTemperature}°`}</span>
      <div className="col-span-1 flex flex-col justify-between mb-2">
        <span>{`${ceilTemperature}°`}</span>
        <span>{`${flooredTemperature}°`}</span>
      </div>
      <div className="col-span-23 grid items-end grid-cols-24 gap-2 mb-2 w-full">
        {
          entries.map((entry) => {
            return (
              <div key={entry.time} style={{height: `${entry.temperature*20}px`}} className="bg-white rounded-t" />
            )
          })
        }
      </div>
      <div className="col-start-2 col-span-23 flex justify-between w-full">
        <span>{format(firstEntryDate, "HH:mm")}</span>
        <span>{format(firstEntryDate, "yyyy-MM-dd")}</span>
        <span>{format(lastEntryDate, "HH:mm")}</span>
      </div>
    </div>
  )
}

interface WeatherChartProps {
  entries: WeatherEntry[]
  maxTemperature: number
  minTemperature: number
}

interface WeatherEntry {
  __typename: string,
  time: string,
  temperature: number,
  type: WeatherType
}

enum WeatherType {
  cloud = "cloud",
  rain = "rain",
  snow = "snow",
  sun = "sun",
}