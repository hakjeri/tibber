import React from "react"

export const Card = ({ img, isBig, title }: CardProps) => {

  const wrapperClass = `bg-white ${ isBig ? "md:col-span-2" : ""}`
  return (
    <div className={wrapperClass}>
      <img src={img} />
      {title && <h2 className="px-4 pt-4">{title}</h2>}
    </div>
  )
}

interface CardProps {
  img: string,
  isBig?: boolean,
  title?: string
}