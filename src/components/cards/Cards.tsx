import React from "react";
import { Card } from "./Card";

export const Cards = () => {
  const cards = [
    { img: "https://picsum.photos/1200/800", isBig: true },
    { img: "https://picsum.photos/600/400?a=1", title: "Direct mailing strategy buzz social proof" },
    { img: "https://picsum.photos/600/400?a=2", title: "Hypotheses value proposition" },
  ]
  return (
    <div className="col-span-12 grid gap-14 grid-cols-1 md:grid-cols-4 px-4 mb-10">
      {cards.map((card) => <Card key={card.img} {...card} />)}
    </div>
  )
}
