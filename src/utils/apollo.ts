import { ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client";

export const initializeApolloClient = (JWT: string) => {
  const client = new ApolloClient({
    ssrMode: true,
    link: createHttpLink({
      uri: 'https://app.tibber.com/v4/gql',
      credentials: 'same-origin',
      headers: {
        authorization: JWT,
      },
    }),
    cache: new InMemoryCache(),
  });
  return client
}