export const getTibberJWT = async () => {
  return fetch("https://app.tibber.com/v4/login.credentials", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: process.env.TIBBER_API_EMAIL,
      password: process.env.TIBBER_API_PASSWORD,
    })
  }).then(response =>  response.json())
}