import { AppProps } from 'next/app'
import '../styles/globals.css'

const Tibber = ({ Component, pageProps }: AppProps) => {
  return <Component {...pageProps} />
}

export default Tibber
