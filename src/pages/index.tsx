import Head from 'next/head'
import { Cards } from '../components/cards/Cards'
import { Content } from '../components/Content'
import { Menu } from '../components/Menu'
import { WeatherChart } from '../components/WeatherChart'

const Home = (weatherData: any) => {
  return (
    <>
      <Menu />
      <div className="grid grid-cols-12 md:container mx-auto">
        <Head>
          <title>Tibber code challenge</title>
          <meta name="description" content="Generated by create next app" />
          <link rel="icon" href="/favicon.ico" />
          <link rel="stylesheet" href="https://assets.ctfassets.net/zq85bj8o2ot3/yNFXCSIOWWBDMZZz4PNuG/949818f745f1ed767fa2cc5f9623832d/silka-semibold.ttf" />
          <link rel="stylesheet" href="https://assets.ctfassets.net/zq85bj8o2ot3/4yTsy8ZrHB4fhIu7MmJIi1/396d647d93ab85a3c028e282e94dcc96/avenir-next-regular.ttf" />
        </Head>
        
        <WeatherChart {...weatherData} />
        <Content />
        <Cards />
      </div>
    </>
  )
}

Home.getInitialProps = async () => {
  const weatherData = await getWeatherData();
  return { ...weatherData }
}

const getWeatherData = async () => {
  try {
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_ORIGIN}/api/weather/a8c210fc-2988-4f06-9fe9-ab1bad9529d5`)
    const weatherData = await response.json();
    return weatherData
  } catch (error) {
    console.error(error);
    return {}
  }
}

export default Home
