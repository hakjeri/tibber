import { ApolloClient, gql, NormalizedCacheObject } from "@apollo/client"
import { NextApiRequest, NextApiResponse } from "next"
import { initializeApolloClient } from "../../../utils/apollo"
import { getTibberJWT } from "../../../utils/requests"

let apolloClient: ApolloClient<NormalizedCacheObject>;

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const homeId = req.query["id"]
  if (!apolloClient) {
    const { token } = await getTibberJWT()
    apolloClient = initializeApolloClient(token)
  }

  const { data } = await apolloClient.query({
    query: gql`
      query Weather {
        me {
          home(id: "${homeId}") {
            weather {
              minTemperature
              maxTemperature
              entries {
                time
                temperature
                type
              }
            }
          }
        }
      }
    `,
  });

  const cleanData = data.me.home.weather
  res.status(200).json(cleanData)
}
