module.exports = {
  purge: ['./src/pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundColor: theme => ({
        ...theme('colors'),
        'dark-blue': "#000323"
      }),
      fontFamily: {
        'Silka': ['Silka', 'sans-serif'],
        'Avenir Next': ['Avenir Next', 'sans-serif'],
      },
      gridColumn: {
        'span-23': 'span 23 / span 23',
        'span-24': 'span 24 / span 24',
        'span-25': 'span 25 / span 25',
      },
      gridTemplateColumns: {
       '24': 'repeat(24, [col-end] 1fr)',
       '25': 'repeat(25, [col-end] 1fr)',
      },
      rotate: {
        "-75": "-75deg",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
